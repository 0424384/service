<?php

namespace app\bootstraps;

use yii\base\BootstrapInterface;

class AppBootstrap implements BootstrapInterface
{
    public function bootstrap($app)
    {
        \Yii::$container->setSingleton('exchangeService', 'app\services\ExchangeService');
    }
}
