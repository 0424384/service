<?php

namespace app\modules\exchange\models;

use app\modules\exchange\behaviors\ExchangedateBehavior;
use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property int|null $r030
 * @property string|null $txt
 * @property string|null $cc
 * @property float|null $rate
 * @property string|null $exchangedate
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%currency}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['r030'], 'integer'],
            [['rate'], 'number'],
            [['exchangedate'], 'safe'],
            [['txt'], 'string', 'max' => 255],
            [['cc'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'r030' => Yii::t('app','R 030'),
            'txt' => Yii::t('app','Txt'),
            'rate' => Yii::t('app','Rate'),
            'exchangedate' => Yii::t('app','Exchangedate'),
        ];
    }

    public function behaviors()
    {
        return [
            ['class' => ExchangedateBehavior::class],
        ];
    }
}
