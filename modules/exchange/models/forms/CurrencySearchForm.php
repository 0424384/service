<?php

namespace app\modules\exchange\models\forms;

use app\modules\exchange\models\search\CurrencySearch;
use \yii\base\Model;

class CurrencySearchForm extends Model
{
    public $currencySearch;

    public function __construct($config = [])
    {
        parent::__construct($config);
        $this->currencySearch = new CurrencySearch();
    }

    public function rules()
    {
        return [
            [['txt'], 'string'],
        ];
    }

    public function setPage($page)
    {
        $this->currencySearch->page = $page;
    }

    public function getResult()
    {
        $dataProvider = $this->currencySearch->search();
        return $dataProvider->getModels();
    }
}