<?php

namespace app\modules\api\controllers;

use app\modules\api\base\BaseServiceController;
use app\modules\exchange\models\forms\CurrencySearchForm;


class DataController extends BaseServiceController
{
    public function actionCurrency($id)
    {
        $model = exchangeService()->currencyRepository->findById($id);
        if (!$model) {
            return $this->response(\Yii::t('app', 'Exchange not found'));
        }
        return $this->response(
            \Yii::t('app', 'Exchange data'),
            'success',
            $model
        );
    }

    public function actionCurrencies($page = '')
    {
        $currencySearch = new CurrencySearchForm();
        $currencySearch->setPage($page);
        return $this->response(
            \Yii::t('app', 'Exchanges data'),
            'success',
            $currencySearch->getResult()
        );
    }
}
