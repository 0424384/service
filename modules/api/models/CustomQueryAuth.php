<?php

namespace app\modules\api\models;

use yii\filters\auth\QueryParamAuth;

class CustomQueryAuth extends QueryParamAuth
{
    public function authenticate($user, $request, $response)
    {
        return $request->get($this->tokenParam) == \Yii::$app->params['token'];
    }
}