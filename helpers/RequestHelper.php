<?php

namespace yii\helpers;

class RequestHelper
{
    public static function sendRequest($method = 'GET', $uri, $data = []) : array
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $uri); // set url to
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 20); // times out after 20s
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method); // set method
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data)); // add fields
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/x-www-form-urlencoded'
        ));
        $result = curl_exec($ch); // run the whole process
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $error = curl_error($ch);
        curl_close($ch);
        return compact('result', 'status', 'error');
    }
}