<?php

namespace app\commands;

use yii\console\Controller;

class UpdateExchangeController extends Controller
{
    public function actionSync()
    {
        exchangeService()->sync();
    }
}
